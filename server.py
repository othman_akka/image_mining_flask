from sklearn.model_selection import train_test_split
from sklearn.linear_model import LogisticRegression
from sklearn.neighbors import KNeighborsClassifier
from sklearn.preprocessing import StandardScaler
from sklearn.tree import DecisionTreeClassifier 
from sklearn.preprocessing import MinMaxScaler
from sklearn.metrics import accuracy_score
from sklearn.decomposition import PCA
from sklearn import preprocessing
from sklearn.svm import SVC
from flask import request
import pandas as pd
import numpy as np
import werkzeug
import mahotas
import joblib
import ntpath
import flask 
import glob
import cv2
import os


app = flask.Flask(__name__)

def color_moments(pic):
    pic=np.double(pic)
    R=pic[:,:,0]
    G=pic[:,:,1]
    B=pic[:,:,2]
    colorFeature=[np.mean(R[:]),np.std(R[:]),np.mean(G[:]),np.std(G[:]),np.mean(B[:]),np.std(B[:])];
    colorFeature=colorFeature/np.mean(colorFeature)
    return colorFeature

def shapeFeatures(pic):
     # Calculate Moments
     moments = cv2.moments(cv2.cvtColor(pic, cv2.COLOR_BGR2GRAY))
     # Calculate Hu Moments
     huMoments = cv2.HuMoments(moments).flatten()
     shapeFeat=huMoments/np.mean(huMoments);
     return shapeFeat

# feature-descriptor-2: histogram
def fd_histogram(pic):
    # convert the image to HSV color-space
    pic = cv2.cvtColor(pic, cv2.COLOR_BGR2HSV)
    # compute the color histogram
    hist  = cv2.calcHist(pic, [0, 1, 2], None, [8, 2, 2], [0, 256, 0, 256, 0, 256])
    # normalize the histogram
    cv2.normalize(hist, hist)
    # return the histogram
    return hist.flatten()

# feature-descriptor-2: Haralick Texture
def fd_haralick(pic):
    # convert the image to grayscale
    gray = cv2.cvtColor(pic, cv2.COLOR_BGR2GRAY)
    # compute the haralick texture feature vector
    haralick = mahotas.features.haralick(gray).mean(axis=0)
    # return the result
    
    return haralick
 
def getFeatures(pic):
    featuresGlbal=np.zeros((209,50))
    
    fv_histogram  = fd_histogram(pic)
    fv_haralick = fd_haralick(pic)
    
    featuresGlbal = np.append(fv_haralick , fv_histogram);
    
    return featuresGlbal


def createFeatures(fold):
    liste = os.listdir(fold)
    liste.remove('masks')
    liste.remove('pictures')
    
    with open('features.csv', 'w') as out:
        for dirct in liste:
            liste2 = os.listdir(fold+'\\'+dirct)
            
            masks = glob.glob(fold+'\\'+dirct+'\\'+liste2[0]+'\\*.png')
            pics = glob.glob(fold+'\\'+dirct+'\\'+liste2[1]+'\\*.png')
            
            for mask ,pic in zip(masks,pics):
                img = cv2.imread(pic)
                msk = cv2.imread(mask,0)
                
                res = cv2.bitwise_and(img,img,mask = msk)
                
                fts = np.append(dirct,getFeatures(res))
                out.write('%s\n'% ','.join(fts))
                
#_______________________________________________________________________________________________________

def unsupervised(fold):
    liste = os.listdir(fold)
    
    masks = glob.glob(fold+'\\'+liste[0]+'\\*.png')
    pics = glob.glob(fold+'\\'+liste[1]+'\\*.png')
    
    targets = []
    features = []

    for uriM ,uriP in zip(masks,pics):
        msk = cv2.imread(uriM,0)
        img = cv2.imread(uriP)
        featsM = msk.ravel()
        featsP = Arrays2Matrix(img)
        targets.append(featsM)
        features.append(featsP)
        
    targets = np.asarray(targets)   
    features = np.asarray(features)
    
#    # create a csv file
#    r= pd.DataFrame({'targets':targets,'features':features})
#    r.to_csv("akka.csv",index=True)
    
    return [features , targets ]
        
        
def YCbCr2Arrays(img):
    imgY = cv2.cvtColor(img, cv2.COLOR_BGR2YCR_CB)
    #extract y cb and cr from pic      
    y=imgY[:,:,0]
    cb=imgY[:,:,1]
    cr=imgY[:,:,2]
    
    #convert y cb and cr to arrays
    arrayY = y.ravel()
    arrayCb = cb.ravel()
    arrayCr = cr.ravel()
    
    return [arrayY,arrayCb,arrayCr]
        
def Arrays2Matrix(img):
    [arrayY,arrayCb,arrayCr] = YCbCr2Arrays(img)
    
    x_train =[]
    for val1,val2,val3 in zip(arrayY,arrayCb,arrayCr):
        array=[val1,val2,val3]
        x_train.append(array)
    return x_train

    
@app.route('/',methods=['GET','POST'])

def hundle_request():
    
    imagefile = request.files['image']
    filename = werkzeug.utils.secure_filename(imagefile.filename)
    print("\nReceived image File name : " + imagefile.filename)
    
    pics = glob.glob('pic_client\\*.png')
    
    for pic in pics:
        os.remove(pic)
    
    imagefile.save("pic_client\\"+filename)
    
    pic_name = filename.split("_")[1:]
    
    name_mask ='mask'; 
    for term in pic_name:
        name_mask +="_"+term
    
    img = cv2.imread("pic_client\\"+filename)
    msk = cv2.imread("allData\\testAll\\mask\\"+name_mask,0)
                
    res = cv2.bitwise_and(img,img,mask = msk)
    
    feature = getFeatures(res)
    
    # create features of data
    #createFeatures("data3")
    
    data = pd.read_csv('features.csv', sep = ',').to_numpy()
    features = data[:,1:]
    targets = data[:,0]  
    
    # train_test_split
    #x_train, x_test, y_train, y_test = train_test_split(features, target, test_size=0.60, random_state=42)
    
    # Build your classifier SVM
    classifier = SVC(C=1.0,kernel='rbf',gamma='scale')
    
    # save the model to disk
    filename = 'modelSVM.sav'
    joblib.dump(classifier, filename)
    
    # Load the pickled model 
    classifier = joblib.load(filename)
    
    classifier.fit(features,targets)
    print("Accuracy SVM : %.2f%%" % (classifier.score(features,targets)*100.0))
   
    # Build your classifier decision tree
    classifier2 = DecisionTreeClassifier(criterion = "gini",random_state = 100,max_depth=3, min_samples_leaf=5)
    
    # save the model to disk
    filename = 'modelDT.sav'
    joblib.dump(classifier2, filename)
    
    # Load the pickled model 
    classifier2 = joblib.load(filename)
    
    classifier2.fit(features,targets)
    print("Accuracy DT : %.2f%%" % (classifier2.score(features,targets)*100.0))
 
    #Build your classifier KNN
    classifier3 = KNeighborsClassifier(n_neighbors=5)
    
    # save the model to disk
    filename = 'modelKNN.sav'
    joblib.dump(classifier3, filename)
    
    # Load the pickled model 
    classifier3 = joblib.load(filename)
    
    classifier3.fit(features,targets)
    print("Accuracy KNN : %.2f%%" % (classifier3.score(features,targets)*100.0))
    
    print(classifier.predict(feature.reshape(1,-1))[0])
    
    return "   "+classifier.predict(feature.reshape(1,-1))[0]

if __name__ == "__main__":
    # debug=True : allow the server to restart itself for each change in the source code
    app.run(host="192.168.43.5", port=5000, debug=True)
    #app.run()
    

    