package com.example.server_side;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.Entity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.util.Base64;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class MainActivity extends AppCompatActivity {
    private EditText ipv4AddressView;
    private EditText nom;
    private EditText prenom;
    private EditText cni;
    private EditText classeView;
    private ImageView imageListe;
    private Button connect_server;
    private Button select_image;
    private Button take_image;
    private String portNumber;
    private String postUrl;
    private static String pathImage;
    private ImageView imageView;
    private Dialog dialog;

    private Button add_dialog;
    private Button cancel_dialog;

    private Button listeV;
    private Button cacherV;
    private ListView listeview;

    private DataBaseHalper dataBaseHalper;
    private Patient patient;

    private ArrayList<Patient> arrayList;
    private AdapterPatient adapeter;

    private static byte[] byteArray;

    private static int RESULT_LOAD_IMAGE = 1;
    private static int REQUEST_IMAGE_CAPTURE = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        dataBaseHalper = new DataBaseHalper(this,null,1);
        patient = new Patient();

        //responseText = findViewById(R.id.responseText);

        connect_server = findViewById(R.id.connect);
        imageView = findViewById(R.id.imgView);

        connect_server.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // adress of server
                ipv4AddressView = findViewById(R.id.IPAddress);
                String ipv4Address = ipv4AddressView.getText().toString();

                // uri
                postUrl= "http://"+ipv4Address+":"+5000+"/";
                //postUrl = "othmanakka.pythonanywhere.com";
                // image
                String filename = pathImage.substring(pathImage.lastIndexOf("/"));
                filename = filename.substring(1,filename.indexOf("."));
                
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inPreferredConfig = Bitmap.Config.RGB_565;

                // Read BitMap by file path
                Bitmap bitmap = BitmapFactory.decodeFile(pathImage, options);
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
                byteArray = stream.toByteArray();

                RequestBody postBodyImage = new MultipartBody.Builder()
                        .setType(MultipartBody.FORM)
                        .addFormDataPart("image", filename+".png", RequestBody.create(MediaType.parse("image/*png"), byteArray))
                        .build();

                //Take note that to send the message to the server, it’s necessary to allow the app to know that the message prepared
                // using the RequestBody class should be sent to the destination URL declared previously
                postRequest(postUrl, postBodyImage);
            }
        });

        select_image = findViewById(R.id.image);
        select_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Create an Intent with action as ACTION_PICK
                Intent intent = new Intent(Intent.ACTION_PICK);
                // Sets the type as image/*. This ensures only components of type image are selected
                intent.setType("image/*");
                startActivityForResult(intent , RESULT_LOAD_IMAGE);
            }
        }
        );

        take_image = findViewById(R.id.imageT);
        take_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                    startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
                }
            }
        });


        listeV = findViewById(R.id.liste);
        listeV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listeview();
            }
       });

        cacherV = findViewById(R.id.cache);
        cacherV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                arrayList.clear();
                listeview.setAdapter(adapeter);
                adapeter.notifyDataSetChanged();
            }
        });
    }
    void listeview(){
        listeview = findViewById(R.id.listview);
        arrayList = new ArrayList<>();
        arrayList.addAll(dataBaseHalper.getAllPatient());
        adapeter = new AdapterPatient(this, R.layout.list, arrayList,pathImage,byteArray);
        listeview.setAdapter(adapeter);
        adapeter.notifyDataSetChanged();
    }

    void postRequest(String postUrl, RequestBody postBody) {
        // This class is the preferred way to establish HTTP connections for sending requests and reading responses
        OkHttpClient client = new OkHttpClient();

        // This class is responsible for mapping the destination URL with the request body
        Request request = new Request.Builder()
                // accepts the URL
                .url(postUrl)
                // accepts the request body
                .post(postBody)
                // the request is built with build()
                .build();
        // send the request via the instance of the OkHttpClient
        // enqueue : This is because there may be other requests waiting to be sent
        client.newCall(request).enqueue(new Callback() {
            @Override
            // Called when the request couldn’t be executed due to cancellation, a connectivity problem, or timeout.
            public void onFailure(Call call, IOException e) {
                // Cancel the post on failure.
                call.cancel();

                // In order to access the TextView inside the UI thread, the code is executed inside runOnUiThread()
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        message("Failed to Connect to Server");
                    }
                });
            }

            @Override
            // Called when the HTTP response is successfully returned by the remote server.
            public void onResponse(Call call, final Response response) throws IOException {
                // In order to access the TextView inside the UI thread, the code is executed inside runOnUiThread()
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        try {
                            //responseText.setText(" ");
                           // message("check response");
                            // convert response body to string

                            String resJson = response.body().string();
                            message(resJson);

                            dialog = new Dialog(MainActivity.this);
                            dialog.setContentView(R.layout.dialogue);

                            nom = dialog.findViewById(R.id.nomDialog);
                            prenom = dialog.findViewById(R.id.prenomDialog);
                            cni = dialog.findViewById(R.id.cniDialog);
                            classeView = dialog.findViewById(R.id.classeDialog);


                            // message hssan
                            classeView.setText(resJson);

                            dialog.show();
                            //responseText.setText(resJson);

                            add_dialog = dialog.findViewById(R.id.addDialog);
                            add_dialog.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    if(!nom.getText().toString().equals("") && !prenom.getText().toString().equals("") && !cni.getText().toString().equals("") && !classeView.getText().toString().equals(""))
                                    {
                                        patient.setNom(nom.getText().toString());
                                        patient.setPrenom(prenom.getText().toString());
                                        patient.setCni(cni.getText().toString());
                                        patient.setClasse(classeView.getText().toString());
                                        patient.setArray(byteArray);



                                        if (dataBaseHalper.addPatient(patient))
                                        {
                                            message("Succeed");
                                            dialog.dismiss();
                                        }
                                        else{
                                            message("Failure");
                                        }
                                    }
                                    else{
                                        message("Forget Field vide");
                                    }
                                }
                            });

                            cancel_dialog = dialog.findViewById(R.id.cancelDialog);
                            cancel_dialog.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    dialog.dismiss();
                                }
                            });
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        });
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK && null != data) {
            //data.getData return the content URI for the selected Image
            Uri selectedImage = data.getData();
            String[] filePathColumn = { MediaStore.Images.Media.DATA };
            // Get the cursor
            Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
            // Move to first row
            cursor.moveToFirst();
            //Get the column index of MediaStore.Images.Media.DATA
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);

            //Gets the String value in the column
            pathImage = cursor.getString(columnIndex);

            cursor.close();
            //set bitmap in class
            //admin_image.setBitmap(BitmapFactory.decodeFile(imgDecodableString));
            // Set the Image in ImageView after decoding the String
            imageView.setImageBitmap(BitmapFactory.decodeFile(pathImage));
        }

        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {

            Uri selectedImage = data.getData();
            String[] filePathColumn = { MediaStore.Images.Media.DATA };
            // Get the cursor
            Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
            // Move to first row
            cursor.moveToFirst();
            //Get the column index of MediaStore.Images.Media.DATA
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);

            //Gets the String value in the column
            pathImage = cursor.getString(columnIndex);

            cursor.close();
            //Bundle extras = data.getExtras();

            //Bitmap imageBitmap = (Bitmap) extras.get("data");
            imageView.setImageBitmap(BitmapFactory.decodeFile(pathImage));
        }
    }

    private String getStringFromBitmap(Bitmap bitmapPicture) {
        final int COMPRESSION_QUALITY = 100;
        String encodedImage;
        ByteArrayOutputStream byteArrayBitmapStream = new ByteArrayOutputStream();
        bitmapPicture.compress(Bitmap.CompressFormat.PNG, COMPRESSION_QUALITY,byteArrayBitmapStream);
        byte[] b = byteArrayBitmapStream.toByteArray();
        encodedImage = Base64.encodeToString(b, Base64.DEFAULT);
        return encodedImage;
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId())
        {
            case R.id.help:
                Intent intent=new Intent(this,help.class);
                startActivity(intent);
                return true;
            case R.id.exit:
                System.exit(0);
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    private void message(String string){
        Toast.makeText(this,string,Toast.LENGTH_LONG).show();
    }
}






