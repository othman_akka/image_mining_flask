package com.example.server_side;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;

import java.util.ArrayList;

public class DataBaseHalper extends SQLiteOpenHelper {
    final static String database = "database3";
    static int a = 0;
    Login login;
    Patient patient;


    public DataBaseHalper(Context context, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, database, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        login = new Login();
        patient = new Patient();

        db.execSQL("create table login(mail text primary key ,password text)");
        db.execSQL("create table pat(nom text,prenom text,cni text,classe text,array BLOB)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        super.onDowngrade(db, oldVersion, newVersion);
    }

    public boolean login(Login login) {
        SQLiteDatabase database = getReadableDatabase();
        Cursor cursor = database.rawQuery("select * from login", null);
        if (cursor.moveToFirst()) {
            while (cursor.isAfterLast() == false) {
                if (cursor.getString(0).equals(login.getMail()) && cursor.getString(1).equals(login.getPassword())) {
                    return true;
                }
                cursor.moveToNext();
            }
        }
        cursor.close();
        database.close();
        return false;
    }

   //____________________________________________________________________________

    public Boolean addLogin(Login login) {
        SQLiteDatabase database = getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("mail", login.getMail());
        values.put("password", login.getPassword());
        if (!login.getMail().equals(" ") && !login.getPassword().equals(" ") && test(login.getMail()) == false) {
            database.insert("login", null, values);
            return true;
        }
        database.close();
        return false;
    }

    public boolean test(String mail) {
        Cursor cursor = getReadableDatabase().rawQuery("select mail from login", null);
        if (cursor.moveToFirst()) {
            while (cursor.isAfterLast() == false) {
                if (cursor.getString(0).equals(mail))
                    return true;
                cursor.moveToNext();
            }
        }
        cursor.close();
        return false;
    }
   // ____________________________________________________________________________

   public Boolean addPatient(Patient patient) {
       SQLiteDatabase database = getWritableDatabase();
       ContentValues values = new ContentValues();
       values.put("nom", patient.getNom());
       values.put("prenom", patient.getPrenom());
       values.put("cni",patient.getCni());
       values.put("classe", patient.getClasse());
       values.put("array",patient.getArray());

       try {
           database.insert("pat", null, values);
           database.close();
           return true;
       }catch (Exception e)
       {
           return false;
       }
   }

    public ArrayList<Patient> getAllPatient() {

        ArrayList<Patient> arrayList = new ArrayList<>();
        Cursor cursor = getReadableDatabase().rawQuery("select * from pat",null);
        if (cursor.moveToFirst()) {
            while (cursor.isAfterLast() == false) {
                patient = new Patient(cursor.getString(0),cursor.getString(1),cursor.getString(2)
                        ,cursor.getString(3),
                        cursor.getBlob(cursor.getColumnIndex("array")));
                arrayList.add(patient);
                cursor.moveToNext();
            }
        }
        cursor.close();
        return arrayList;
    }
}
