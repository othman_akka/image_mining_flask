package com.example.server_side;

import android.graphics.Bitmap;

public class Patient {
    private String nom;
    private String prenom;
    private String cni;
    private String classe;
    private byte[] array;

    public Patient() {
    }

    public Patient(String nom, String prenom, String cni, String classe) {
        this.nom = nom;
        this.prenom = prenom;
        this.cni = cni;
        this.classe = classe;
    }

    public Patient(String nom, String prenom, String cni, String classe, byte[] array) {
        this.nom = nom;
        this.prenom = prenom;
        this.cni = cni;
        this.classe = classe;
        this.array = array;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getCni() {
        return cni;
    }

    public void setCni(String cni) {
        this.cni = cni;
    }

    public String getClasse() {
        return classe;
    }

    public void setClasse(String classe) {
        this.classe = classe;
    }

    public byte[] getArray() {
        return array;
    }

    public void setArray(byte[] array) {
        this.array = array;
    }
}
