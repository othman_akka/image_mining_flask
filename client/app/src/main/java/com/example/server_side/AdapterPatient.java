package com.example.server_side;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.ArrayList;

public class AdapterPatient extends ArrayAdapter<Patient> {
    Context context;
    int resource;
    static ArrayList<Patient> arrayAdapter;
    TextView nom,prenom,cni,classe;
    ImageView image;
    String path;
    byte[] array;

    public AdapterPatient(Context context, int resource, ArrayList<Patient> arrayAdapter ,String path ,byte[] array) {
        super(context, resource, arrayAdapter);
        this.context = context;
        this.resource = resource;
        this.arrayAdapter = arrayAdapter;
        this.path = path;
        this.array = array;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater=((Activity)context).getLayoutInflater();
        View view = inflater.inflate(resource,parent,false);
        nom = view.findViewById(R.id.nomList);
        prenom = view.findViewById(R.id.prenomList);
        cni = view.findViewById(R.id.cniList);
        classe = view.findViewById(R.id.classeList);
        image =view.findViewById(R.id.imglist);

        nom.setText(arrayAdapter.get(position).getNom());
        prenom.setText(arrayAdapter.get(position).getPrenom());
        cni.setText(arrayAdapter.get(position).getCni());
        classe.setText(arrayAdapter.get(position).getClasse());
        Bitmap bitmap = BitmapFactory.decodeByteArray(arrayAdapter.get(position).getArray(), 0,
                arrayAdapter.get(position).getArray().length);
        image.setImageBitmap(bitmap);
        return view;
    }
}
