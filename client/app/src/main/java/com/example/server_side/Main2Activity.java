package com.example.server_side;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;


public class Main2Activity extends AppCompatActivity {
    private Button signup;
    private Button login;

    private EditText mail;
    private EditText password;

    private ImageView facebook;
    private ImageView gmail;

    private Login loginC;
    private DataBaseHalper dataBaseHalper;

    //private List<AuthUI.IdpConfig> providers;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);


        //providers = Arrays.asList(
                //new AuthUI.IdpConfig.EmailBuilder().build(), // email builder
               // new AuthUI.IdpConfig.FacebookBuilder().build(), // facebook builder
               // new AuthUI.IdpConfig.GoogleBuilder().build()// goole builder
        //);
        //ShowSignInOptions()

        // surcharge class Login
        loginC = new Login();
        dataBaseHalper = new DataBaseHalper(this,null,1);

        // arriver to activity of SIGNUP for Sign up
        signup = findViewById(R.id.btnSignup);
        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toIntent();
            }
        });

        login = findViewById(R.id.button3);
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mail = findViewById(R.id.mailE);
                password = findViewById(R.id.passwordE);
                if(!mail.getText().toString().equals("") && !password.getText().toString().equals("") )
                {
                    loginC.setMail(mail.getText().toString());
                    loginC.setPassword(password.getText().toString());

                    if(dataBaseHalper.login(loginC)==true){
                        Intent intent=new Intent(Main2Activity.this,MainActivity.class);
                        startActivity(intent);
                        finish();

                    }
                    else{
                        message("this account don't exist");
                        mail.setText("");
                        password.setText("");
                    }
                }
                else
                {
                    mail.setText("");
                    password.setText("");
                    message("Field empty");
                }
            }

            private void finish() {
                this.finish();
            }
        });

        facebook = findViewById(R.id.facebook);
        facebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                message("kkk");
            }
        });
    }

    //private void ShowSignInOptions() {
        //AuthUI.getInstance().createSignInIntentBuilder()

    //}

    private void toIntent(){
        Intent intent = new Intent(this, Main3Activity.class);
        startActivity(intent);
        finish();
    }
    private void message(String string){
        Toast.makeText(this,string,Toast.LENGTH_LONG).show();
    }

}
