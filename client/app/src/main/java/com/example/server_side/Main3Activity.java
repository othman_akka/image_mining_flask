package com.example.server_side;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

public class Main3Activity extends AppCompatActivity {
    private Button login;
    private Button signup;

    private EditText mail;
    private EditText password;
    private EditText passwordC;

    private ImageView facebook;
    private ImageView gmail;

    private Login loginC;
    private DataBaseHalper dataBaseHalper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);

        // surcharge class Login
        loginC = new Login();
        dataBaseHalper =new DataBaseHalper(this,null,1);

        // arriver to activity of SIGNUP for Sign up
        login = findViewById(R.id.btnLoginE);
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toIntent();
            }
        });

        signup = findViewById(R.id.button33);
        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mail = findViewById(R.id.mailEE);
                password = findViewById(R.id.passwordEE);
                passwordC = findViewById(R.id.passwordConfirmEE);
                if(!mail.getText().toString().equals(" ") && !password.getText().toString().equals(" "))
                {
                    if(password.getText().toString().equals(passwordC.getText().toString()))
                    {
                        loginC.setMail(mail.getText().toString());
                        loginC.setPassword(password.getText().toString());
                        if(dataBaseHalper.addLogin(loginC))
                        {
                            message("Success");
                            Intent intent =new Intent(Main3Activity.this,Main2Activity.class);
                            startActivity(intent);
                            finish();
                        }
                        else
                        {
                            message("Failure");
                        }
                    }
                }
                else{
                    mail.setText("");
                    password.setText("");
                    message("Field empty");
                }
            }
            private void finish() {
                this.finish();
            }
        });

        facebook = findViewById(R.id.facebookE);
        facebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //message();
            }
        });
    }

    private void toIntent(){
        Intent intent = new Intent(this, Main2Activity.class);
        startActivity(intent);
        finish();
    }
    private void message(String string){
        Toast.makeText(this,string,Toast.LENGTH_LONG).show();
    }
}
